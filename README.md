# ABOUT

The _Run-script_ is an external tool for the
[MATE Desktop Environment](http://mate-desktop.org/)'s
code editor: 
[Pluma](https://github.com/mate-desktop/pluma).

# FEATURES

_Run-script_ offers the next abilities:
1. run scripts
such as
([shell](https://en.wikipedia.org/wiki/Bourne_shell)
or [Python](https://www.python.org/) scripts)
using the
_Service > External tools > Run the script_
menu item or by pressing
_[C-j]_ (_Control_ and _J_) keys simultaneously;
1. if the current file is not an executable file,
the script will ask you if you want to add an
_execute_ permission to the file;
1. tool also supports command line arguments.

# INSTALLATION

To install the tool you should download the
[Run-script](Run-script) file and put it in
the _~/.config/pluma/tools/_ directory on your
computer.

```bash
wget -O ~/.config/pluma/tools/Run-script https://gitlab.com/vasilyb/pluma-tool-run-script/raw/master/Run-script
```

# DEPENDENCIES

1. `zenity`
